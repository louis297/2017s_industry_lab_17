package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {

    SimpleSpellChecker checker ;
    @Before
    public void setUp(){
        try {
            checker = new SimpleSpellChecker(new Dictionary(), "For this exercise, you’ll write unit tests for the Dictionary and SimpleSpellChecker");
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void TestConstructor(){
        try {
            new SimpleSpellChecker(new Dictionary(), null);
            fail();
        }  catch (InvalidDataFormatException e) {
        }
    }

    @Test
    public void TestGetFrequencyOfWord(){
        try{
            assertEquals(2, checker.getFrequencyOfWord("For"));
            assertEquals(1, checker.getFrequencyOfWord("you"));
            assertEquals(0, checker.getFrequencyOfWord("asdfasdf"));
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
            fail();
        }
//        try{
//            assertEquals(2, checker.getFrequencyOfWord("awefafasdf"));
//            fail();
//        } catch (InvalidDataFormatException e){
//
//        } catch (NullPointerException e){
//            throw e;
//        }
    }

    @Test
    public void TestGetMisspelledWords(){
//        ll, tests, dictionary, simplespellchecker
        String[] toTest = {"ll", "tests", "dictionary", "simplespellchecker"};
        Set<String> ret = new HashSet<>(checker.getMisspelledWords());
        for (String s : toTest) {
            assertTrue(ret.contains(s));
        }
        assertFalse(ret.contains("correct"));
        assertFalse(ret.contains(""));
    }

    @Test
    public void TestGetUniqueWords(){
//        ll
//                the
//        unit
//                tests
//        dictionary
//                and
//        for
//        this
//        exercise
//                write
//        simplespellchecker
//                you
        String[] toTest = {"ll", "the", "unit", "tests", "and", "for", "this",
                "exercise", "write", "you", "dictionary", "simplespellchecker"};
        Set<String> ret = new HashSet<>(checker.getUniqueWords());
        for (String s : toTest) {
            assertTrue(ret.contains(s));
        }
        assertFalse(ret.contains("correct"));
        assertFalse(ret.contains(""));

    }
    @Test
    public void TestFoundationsOfMathematics() {
        assertEquals(2, 1 + 1);
    }

}