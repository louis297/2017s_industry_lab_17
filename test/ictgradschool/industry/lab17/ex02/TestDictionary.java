package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Implement this class.
 */
public class TestDictionary {

    Dictionary myDictionary;
    @Before
    public void setUp(){
        myDictionary = new Dictionary();
    }

//    @Test
//    public void testTrueIsTrue() {
//        assertEquals(true, true);
//    }

    @Test
    public void SpellingCorrectTest(){
        assertEquals(true, myDictionary.isSpellingCorrect("decision"));
        assertEquals(true, myDictionary.isSpellingCorrect("separate"));
        assertEquals(false, myDictionary.isSpellingCorrect("saparate"));
        assertEquals(false, myDictionary.isSpellingCorrect("awefa"));
        assertEquals(false, myDictionary.isSpellingCorrect(""));
//        assertEquals(false, myDictionary.isSpellingCorrect(null));
    }
}