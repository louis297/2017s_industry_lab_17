package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
        assertEquals(myRobot.row(), myRobot.currentState().row);
        assertEquals(myRobot.column(), myRobot.currentState().column);
    }

    @Test
    public void testRow(){
        assertEquals(myRobot.currentState().row, myRobot.row());

    }


    @Test
    public void testTurn() {
        for (int i = 0; i < 4; i++) {
            Robot.Direction currentDirection = myRobot.getDirection();
            int currentRow = myRobot.row();
            int currentColumn = myRobot.column();
            myRobot.turn();


            assertEquals(currentColumn, myRobot.column());
            assertEquals(currentRow, myRobot.row());
            switch (currentDirection) {
                case North:
                    assertEquals(Robot.Direction.East, myRobot.getDirection());
                    break;
                case East:
                    assertEquals(Robot.Direction.South, myRobot.getDirection());
                    break;
                case South:
                    assertEquals(Robot.Direction.West, myRobot.getDirection());
                    break;
                case West:
                    assertEquals(Robot.Direction.North, myRobot.getDirection());
                    break;
            }
        }

    }

    @Test
    public void testLegalMove() {
//        Move north
        boolean atEdge = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atEdge = myRobot.currentState().row == 1;
            assertTrue(atEdge);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }

//        Move east
        myRobot.turn();
        atEdge = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atEdge = myRobot.currentState().column == 10;
            assertTrue(atEdge);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().column);
        }

//        Move south
        myRobot.turn();
        atEdge = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atEdge = myRobot.currentState().row == 10;
            assertTrue(atEdge);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }

//        Move west
        myRobot.turn();
        atEdge = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atEdge = myRobot.currentState().column == 1;
            assertTrue(atEdge);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().column);
        }
    }

    @Test
    public void testBackTrack(){
        myRobot.turn();
        myRobot.backTrack();
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

        myRobot.backTrack();
        myRobot.backTrack();
        myRobot.backTrack();
    }
}
