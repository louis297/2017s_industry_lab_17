package ictgradschool.industry.lab17.ex04;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestExercise04 {
    Exercise04 ex04;

    @Before
    public void setUp(){
        ex04 = new Exercise04();
    }

    @Test
    public void TestAreaOfRectangle(){
        assertEquals(4, ex04.AreaOfRectangle(2,2));
        assertEquals(8, ex04.AreaOfRectangle(2,4));
    }

    @Test
    public void TestAreaOfCircle(){
        assertEquals(3, ex04.AreaOfCircle(1));
        assertEquals(13, ex04.AreaOfCircle(2));

    }

}
