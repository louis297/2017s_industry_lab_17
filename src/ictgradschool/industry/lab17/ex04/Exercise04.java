package ictgradschool.industry.lab17.ex04;

import ictgradschool.Keyboard;

public class Exercise04 {
    public int AreaOfRectangle(double width, double height){
        return (int)Math.round(width*height);
    }

    public int AreaOfCircle(double radius){
        return (int)Math.round(radius*radius*Math.PI);
    }

    public static void main(String[] args) {
        Exercise04 ex04 = new Exercise04();
        // input
        System.out.println("Welcome to Shape Area Calculator!\n");
        System.out.print("Enter the width of the rectangle: ");
        double width = Double.parseDouble(Keyboard.readInput());
        System.out.print("Enter the height of the rectangle: ");
        double height = Double.parseDouble(Keyboard.readInput());
        int radius = (int) Math.round(Math.random()*20) + 10;
        System.out.println("The radius of the circle is: " + radius);
        System.out.println();

        // call
        int ar = ex04.AreaOfRectangle(width, height);
        int ac = ex04.AreaOfCircle(radius);

        // compare

        System.out.println("The smaller area is: " + Math.min(ar, ac));
    }
}
